<?php

namespace Adoptavia\Teamwork;

use GuzzleHttp\Client as Guzzle;
use Illuminate\Support\ServiceProvider;

class TeamworkServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('adoptavia.teamwork', function ($app) {
            $client = new \Adoptavia\Teamwork\Client(new Guzzle,
                $app['config']->get('services.teamwork.key'),
                $app['config']->get('services.teamwork.url')
            );

            return new \Adoptavia\Teamwork\Factory($client);
        });

        $this->app->bind('Adoptavia\Teamwork\Factory', 'adoptavia.teamwork');
    }
}